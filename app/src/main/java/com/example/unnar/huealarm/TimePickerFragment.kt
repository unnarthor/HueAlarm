package com.example.unnar.huealarm

import android.app.Dialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.util.Log
import android.widget.TimePicker
import java.util.*

/**
 * Created by unnar on 08/12/17.
 */ 

class TimeDialogHandler : android.support.v4.app.DialogFragment(), TimePickerDialog.OnTimeSetListener {

    var onTimePicked: ((String) -> Unit)? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        var c = Calendar.getInstance()
        var hour: Int = c.get(Calendar.HOUR_OF_DAY)
        var minute: Int = c.get(Calendar.MINUTE)
        return TimePickerDialog(activity, this, hour, minute, true)
    }

    override fun onTimeSet(p0: TimePicker?, p1: Int, p2: Int) {
        Log.d("TimeDialogHandler", "Time $p1, $p2")
        val hour: String = p1.toString().padStart(2, '0')
        val minutes: String = p2.toString().padStart(2, '0')
        onTimePicked?.invoke("$hour:$minutes")

    }
}