package com.example.unnar.huealarm.db.Alarm

import android.arch.persistence.room.ColumnInfo

/**
 * Created by unnar on 13/12/17.
 *
 * Data class containing information regarding philips hue.
 */

data class AlarmHue(@ColumnInfo(name = "hue_enabled") var enabled: Boolean, var duration: Int)