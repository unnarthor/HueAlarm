package com.example.unnar.huealarm.db.Alarm
import android.arch.persistence.room.*
import android.content.Context
import com.example.unnar.huealarm.db.SingletonHolder

/**
 * Created by unnar on 12/12/17.
 *
 * Data class defining the alarm.
 */

@Entity
data class Alarm(
        var name: String,
        var enabled: Boolean,
        @Embedded var time: AlarmTime,
        @Embedded var days: AlarmDays,
        @Embedded var hue: AlarmHue) {
    @PrimaryKey(autoGenerate = true) var id: Long = 0
}