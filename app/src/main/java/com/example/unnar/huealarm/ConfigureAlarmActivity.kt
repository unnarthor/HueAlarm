package com.example.unnar.huealarm

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import com.example.unnar.huealarm.db.Alarm.*
import android.os.AsyncTask
import android.provider.ContactsContract.CommonDataKinds.Note
import android.os.AsyncTask.execute
import android.widget.Toast


class ConfigureAlarmActivity : AppCompatActivity() {

    val TAG: String = "ConfigureAlarmActivity"

    private lateinit var alarmDB: AlarmDatabase

    private var alarm = Alarm("TestAlarm", true,
            AlarmTime(10, 45),
            AlarmDays(false, false, false, false, false, false, false),
            AlarmHue(true, 30))

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_configure_alarm)

        alarmDB = AlarmDatabase.getInstance(applicationContext)

        alarmDB.insertAlarmAsync(alarm, {
            Log.d(TAG, "inserted----------------------------")
        })

        alarmDB.countAlarmsAsync { Log.d(TAG, "COUNT $it ----------------------") }

        alarmDB.getAllAlarmsAsync {
            for (a in it) {
                Log.d(TAG, a.toString())
            }

        }
    }

    fun editTime( @Suppress("UNUSED_PARAMETER") view : View) {
        val handler = TimeDialogHandler()
        handler.onTimePicked = { newTime ->
            val time = findViewById<TextView>(R.id.alarmTime) as TextView
            time.text = newTime
            Log.d("ConfigureAlarmActivity", alarm.toString())
        }
        handler.show(
                supportFragmentManager ,
                "time_picker"
        )
    }
}
