package com.example.unnar.huealarm.db.Alarm

/**
 * Created by unnar on 13/12/17.
 *
 * Data class containing information about alarm days.
 */

data class AlarmDays(var mon: Boolean,
                     var tue: Boolean,
                     var wed: Boolean,
                     var thu: Boolean,
                     var fri: Boolean,
                     var sat: Boolean,
                     var sun: Boolean)