package com.example.unnar.huealarm.db.Alarm

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import android.os.AsyncTask
import android.util.Log
import com.example.unnar.huealarm.db.SingletonHolder

/**
 * Created by unnar on 13/12/17.
 *
 * Database class, defines the alarm database and some async helper functions to access it.
 */

@Database(entities = arrayOf(Alarm::class), version = 1, exportSchema = false)
abstract class AlarmDatabase : RoomDatabase() {

    abstract fun alarmDao(): AlarmDao

    fun insertAlarmAsync(alarm: Alarm, callback: () -> Unit) {
        class AsyncInsert : AsyncTask<Alarm, Unit, Unit>() {
            override fun doInBackground(vararg alarms: Alarm) {
                for (a in alarms) {
                    AlarmDatabase.instance?.alarmDao()?.insertAlarm(a)
                }
                return Unit
            }

            override fun onPostExecute(unit: Unit) {
                callback()
            }
        }
        AsyncInsert().execute(alarm)
    }

    fun getAllAlarmsAsync(callback: (List<Alarm>) -> Unit) {
        class AsyncCount : AsyncTask<Unit, Unit, List<Alarm>>() {
            override fun doInBackground(vararg unit: Unit): List<Alarm> {
                return AlarmDatabase.instance?.alarmDao()?.getAllAlarms() ?: ArrayList()
            }

            override fun onPostExecute(alarms: List<Alarm>) {
                callback(alarms)
            }
        }
        AsyncCount().execute()
    }

    fun countAlarmsAsync(callback: (Int) -> Unit) {
        class AsyncCount : AsyncTask<Unit, Unit, Int>() {
            override fun doInBackground(vararg unit: Unit): Int {
                return AlarmDatabase.instance?.alarmDao()?.countAlarms() ?: 0
            }

            override fun onPostExecute(count: Int) {
                callback(count)
            }
        }
        AsyncCount().execute()
    }

    companion object : SingletonHolder<AlarmDatabase, Context>({
        Room.databaseBuilder(it.applicationContext,
                AlarmDatabase::class.java, "Alarm.db")
                .build()
    })
}