package com.example.unnar.huealarm.db.Alarm

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query

/**
 * Created by unnar on 13/12/17.
 *
 * Class contains methods to access the Alarm database.
 */
@Dao
interface AlarmDao {

    @Query("SELECT * FROM alarm")
    fun getAllAlarms(): List<Alarm>

    @Insert(onConflict = REPLACE)
    fun insertAlarm(task: Alarm)

    @Query("SELECT COUNT(*) FROM alarm")
    fun countAlarms(): Int
}