package com.example.unnar.huealarm.db.Alarm

/**
 * Created by unnar on 13/12/17.
 *
 * Data class containing alarm time.
 */

data class AlarmTime(var hour: Int, var min: Int)