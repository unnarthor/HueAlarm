package com.example.unnar.huealarm

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.TimePicker
import com.ikovac.timepickerwithseconds.MyTimePickerDialog
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


class MainActivity : AppCompatActivity() {

    private lateinit var linearLayoutManager: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //val myToolbar = findViewById<View>(R.id.my_toolbar) as Toolbar
        //setSupportActionBar(myToolbar)

        //linearLayoutManager = LinearLayoutManager(this)
        //recyclerViewAlarm.layoutManager = linearLayoutManager
    }

    fun newAlarm(v: View) {
        val intent = Intent(this, ConfigureAlarmActivity::class.java)
        startActivity(intent)
    }
}
